package com.example.employeetask.base;

import android.app.Application;
import android.content.Context;

import com.example.employeetask.data.AppDatabase;


public class EmployeeApplication extends Application {

    private static Application applicationContext;
    private static EmployeeApplication employeeApplication;

    public static Context getAppContext() {
        return applicationContext;
    }

    public static synchronized EmployeeApplication getInstance() {
        if (employeeApplication == null)
            employeeApplication = new EmployeeApplication();
        return employeeApplication;
    }

    private static synchronized void setInstance(EmployeeApplication app) {
        applicationContext = app;
        employeeApplication = app;
    }

    public static AppDatabase getDataBaseInstance() {
        return AppDatabase.getDatabase(getInstance());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
    }
}
