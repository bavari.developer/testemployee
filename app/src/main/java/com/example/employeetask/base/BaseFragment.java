package com.example.employeetask.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;

public abstract class BaseFragment<T extends ViewDataBinding, V extends ViewModel>
        extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
    }

    @Override
    public void onStart() {
        super.onStart();
        initForStart();
    }

    /**
     * Starting the Fragment will call this method -
     * Management for Fragments Only
     */
    public abstract void initForStart();
    /**
     * The View Used for Inflating XML Resource
     */
    protected View rootView;
    /**
     * ViewModel Class Inflated
     */
    protected V mViewModel;
    /**
     * ViewDataBinding Class Inflated
     */
    protected T mViewDataBinding;
    /**
     * Abstracted method to get ViewModel
     *
     * @return 'V' ViewModel
     */
    public abstract V getViewModel();

    /**
     * Inflating Layout Resource
     *
     * @return Layout resource (R.layout.__)
     */
    @LayoutRes
    public abstract int getLayout();

    /**
     * Special Method used for Clearing Resources Used in
     * Initializing the Fragment and actions inside it
     */
    public abstract void clearVariables();

    /**
     * Get the Databinding Variable - as assinged inside Variable
     * with data Tag in the XML resource
     *
     * @return BR.__
     */
    public abstract int getBindingVariable();


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.setLifecycleOwner(getLifeCycleOwner());
        mViewDataBinding.executePendingBindings();
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Must assign the current Fragment as LifeCycleOwner of the ViewModel
     *
     * @return - the current fragment
     */
    public abstract Fragment getLifeCycleOwner();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null) {
            return rootView;
        } else {
            setHasOptionsMenu(false);
            mViewDataBinding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
            rootView = mViewDataBinding.getRoot();
        }
        return rootView;
    }


    @Override
    public void onDestroyView() {
        clearVariables();
        super.onDestroyView();
    }
}
