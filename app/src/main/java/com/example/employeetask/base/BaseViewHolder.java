package com.example.employeetask.base;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void onBind(T data, int position);

    public interface RecyclerClick<T> {
        void onClick(T object, int position);
    }
}
