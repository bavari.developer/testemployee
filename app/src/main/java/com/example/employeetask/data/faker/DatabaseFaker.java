package com.example.employeetask.data.faker;

import com.example.employeetask.base.EmployeeApplication;
import com.example.employeetask.data.entity.Department;
import com.example.employeetask.data.repository.DepartmentRepository;
import com.example.employeetask.data.repository.EmployeeRepository;
import com.example.employeetask.utils.SessionManager;

/**
 * Simple Class For filling database with data for the purpose of
 * filling DB and show results.
 */
public class DatabaseFaker {

    public static final String[] DEPARTS = new String[]{"Building", "Decoration", "Rinalization", "Epic"};

    public DatabaseFaker() {
        SessionManager.init(EmployeeApplication.getAppContext());
        if (!SessionManager.getDBInit()) {
            SessionManager.setDBInit();

            // fill department
            DepartmentRepository departmentRepository = new DepartmentRepository(EmployeeApplication.getDataBaseInstance().getDepartment());
            EmployeeRepository employeeRepository = new EmployeeRepository(EmployeeApplication.getDataBaseInstance().getEmployee());
            for (int index = 0; index < DEPARTS.length; index++) {
                departmentRepository.insertNewDepartment(new Department(0, DEPARTS[index]));
            }
        }

//
//
//        for (int index = 0; index < DEPARTS.length; index++) {
//            for (int eIndex = 0; eIndex < 5; eIndex++) {
//                employeeRepository.insertEmployee(new Employee());
//            }
//        }

    }
}
