package com.example.employeetask.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.employeetask.data.entity.Department;

import java.util.List;

@Dao
public interface DepartmentDao {

    /**
     * Get Department by id
     */
    @Query("select * from department where ID=:value")
    LiveData<Department> getDepartmentById(int value);


    /**
     * Get All Departments
     */
    @Query("select * from department")
    LiveData<List<Department>> getAllDepartment();
    /**
     * Get All Departments
     */
    @Query("select * from department")
    List<Department> getAllDepartmentNotLiveData();

    /**
     * Base Insertion
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long saveData(Department obj);

    @Update
    int updateData(Department obj);

}
