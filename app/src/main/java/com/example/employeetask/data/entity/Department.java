package com.example.employeetask.data.entity;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "department")
public class Department extends BaseObservable implements Serializable {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int ID;
    @NonNull
    private String Name;
    @NonNull
    private double Server_Date_Time = new Date(System.currentTimeMillis()).getTime();
    @NonNull
    private long DateTime_UTC = new Date(System.currentTimeMillis()).getTime();
    @NonNull
    private long Update_DateTime_UTC = new Date(System.currentTimeMillis()).getTime();

    public int getID() {
        return ID;
    }

    public Department() {
    }

    public String getIdAsString() {
        return String.valueOf(getID());
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getServer_Date_Time() {
        return Server_Date_Time;
    }

    public void setServer_Date_Time(double server_Date_Time) {
        Server_Date_Time = server_Date_Time;
    }

    public long getDateTime_UTC() {
        return DateTime_UTC;
    }

    public void setDateTime_UTC(long dateTime_UTC) {
        DateTime_UTC = dateTime_UTC;
    }

    public long getUpdate_DateTime_UTC() {
        return Update_DateTime_UTC;
    }

    public void setUpdate_DateTime_UTC(long update_DateTime_UTC) {
        Update_DateTime_UTC = update_DateTime_UTC;
    }

    public Department(int ID, String name) {
        this.ID = ID;
        Name = name;
    }
}
