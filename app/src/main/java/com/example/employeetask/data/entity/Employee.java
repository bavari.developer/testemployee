package com.example.employeetask.data.entity;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import java.io.Serializable;

/**
 * Main Entity for Employee where it contains all the details of the employee
 */
@Entity(tableName = "employee", primaryKeys = "ID")
public class Employee implements Serializable {
    @NonNull
    private int ID;
    @NonNull
    private int Department_ID;
    @NonNull
    private double Server_Date_Time;
    @NonNull
    private long DateTime_UTC;
    @NonNull
    private long Update_DateTime_UTC;
    @NonNull
    private String First_Name;
    @NonNull
    private String Last_Name;
    @NonNull
    private String Email;
    @NonNull
    private String Mobile_Number;
    @NonNull
    private String Password; //with Encryption)
    @NonNull
    private int Gender;
    @NonNull
    private String Address;
    @Nullable
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] Photo;


    private String department_name;

    public Employee() {
    }


    @Ignore
    public Employee(int department_ID, @NonNull String first_Name,
                    @NonNull String last_Name,
                    @NonNull String email,
                    @NonNull String mobile_Number,
                    @NonNull String password,
                    int gender,
                    @NonNull String address,
                    @Nullable byte[] photo) {
        Department_ID = department_ID;
        First_Name = first_Name;
        Last_Name = last_Name;
        Email = email;
        Mobile_Number = mobile_Number;
        Password = password;
        Gender = gender;
        Address = address;
        Photo = photo;
    }

    @Nullable
    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(@Nullable String department_name) {
        this.department_name = department_name;
    }

    public int getID() {
        return ID;
    }


    public void setID(int ID) {
        this.ID = ID;
    }

    public int getDepartment_ID() {
        return Department_ID;
    }

    public void setDepartment_ID(int department_ID) {
        Department_ID = department_ID;
    }

    public double getServer_Date_Time() {
        return Server_Date_Time;
    }

    public void setServer_Date_Time(double server_Date_Time) {
        Server_Date_Time = server_Date_Time;
    }

    public long getDateTime_UTC() {
        return DateTime_UTC;
    }

    public void setDateTime_UTC(long dateTime_UTC) {
        DateTime_UTC = dateTime_UTC;
    }

    public long getUpdate_DateTime_UTC() {
        return Update_DateTime_UTC;
    }

    public void setUpdate_DateTime_UTC(long update_DateTime_UTC) {
        Update_DateTime_UTC = update_DateTime_UTC;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile_Number() {
        return Mobile_Number;
    }

    public void setMobile_Number(String mobile_Number) {
        Mobile_Number = mobile_Number;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public byte[] getPhoto() {
        return Photo;
    }

    public void setPhoto(byte[] photo) {
        Photo = photo;
    }
}
