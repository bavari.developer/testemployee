package com.example.employeetask.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.employeetask.data.entity.Employee;

import java.util.List;

/**
 * Basic functions for Employee table which (in addition to the main operations of Add, Delete, Update)
 * contains also custom Get Functions
 */
@Dao
public interface EmployeeDao {
    /**
     * Get Employee by ID
     *
     * @param value - employee id
     * @return employee
     */
    @Query("select employee.*, department.Name as 'department_name' from employee " +
            "inner join department on department.ID = employee.Department_ID " +
            "where employee.ID=:value")
    LiveData<Employee> getEmployeeByID(int value);


    /**
     * Get Employee by department ID
     *
     * @param value - department id
     * @return list of employees
     */
    @Query("select * from employee where Department_ID=:value")
    LiveData<List<Employee>> getEmployeeByDepartmentID(int value);


    /**
     * Base Insertion
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long saveData(Employee obj);


    @Query("select employee.*, department.Name as'department_name'from employee " +
            "inner join department on department.ID = employee.Department_ID")
    LiveData<List<Employee>> getAllEmployees();


    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateData(Employee objects);

    @Delete
    int deleteEmployee(Employee employee);
}
