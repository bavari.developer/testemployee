package com.example.employeetask.data;

import android.app.Application;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.employeetask.data.dao.DepartmentDao;
import com.example.employeetask.data.dao.EmployeeDao;
import com.example.employeetask.data.entity.Department;
import com.example.employeetask.data.entity.Employee;

import static com.example.employeetask.utils.AppConstants.DatabaseConstants.DB_NAME;
import static com.example.employeetask.utils.AppConstants.DatabaseConstants.DB_VERSION;

@Database(entities = {Employee.class, Department.class}, version = DB_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    /**
     * Instance private
     */
    private static AppDatabase appDatabase;

    /**
     * Constructor - Empty
     */
    public AppDatabase() {

    }

    /**
     * Main Constructor for the Database which is also Synchronized
     * All synchronized blocks synchronized on the same object can only have one thread executing
     * inside them at the same time. All other threads attempting to enter the synchronized
     * block are blocked until the thread inside the synchronized block exits the block
     *
     * @return
     */
    public static synchronized AppDatabase getDatabase(Application appContext) {
        if (appDatabase == null) {
            synchronized (AppDatabase.class) {
                appDatabase = Room.databaseBuilder(appContext,
                        AppDatabase.class, DB_NAME)
                        .allowMainThreadQueries()
                        .build();
            }
        }
        return appDatabase;
    }

    /**
     * For Employee Table
     *
     * @return -
     */
    public abstract EmployeeDao getEmployee();

    /**
     * For Departments table
     *
     * @return
     */
    public abstract DepartmentDao getDepartment();

}
