package com.example.employeetask.data.repository;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.employeetask.data.dao.DepartmentDao;
import com.example.employeetask.data.entity.Department;

import java.util.List;

/**
 * Using Manual DI for Satisfing the dependencies of this class
 */
public class DepartmentRepository {

    private final DepartmentDao departmentDao;
    private LiveData<List<Department>> allDepartments;


    /**
     * Public Constrcutor for providing the DI
     *
     * @param departmentDao
     */
    public DepartmentRepository(DepartmentDao departmentDao) {
        this.departmentDao = departmentDao;
        this.allDepartments = departmentDao.getAllDepartment();
    }

    public void updateDepartment(Department department) {
        new UpdateDepartment().execute(department);
    }

    public void insertNewDepartment(Department department) {
        new InsertNewDepartmemt().execute(department);
    }

    public LiveData<Department> getDepartmentById(int id) {
        return departmentDao.getDepartmentById(id);
    }

    public LiveData<List<Department>> getAllDepartments() {
        return this.allDepartments;
    }

    public class UpdateDepartment extends AsyncTask<Department, Void, Integer> {
        @Override
        protected Integer doInBackground(Department... departments) {
            return departmentDao.updateData(departments[0]);
        }
    }

    private class InsertNewDepartmemt extends AsyncTask<Department, Void, Long> {
        @Override
        protected Long doInBackground(Department... departments) {
            return departmentDao.saveData(departments[0]);
        }
    }


}
