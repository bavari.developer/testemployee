package com.example.employeetask.data.repository;


import android.os.AsyncTask;

import androidx.annotation.MainThread;
import androidx.lifecycle.LiveData;

import com.example.employeetask.data.dao.EmployeeDao;
import com.example.employeetask.data.entity.Employee;

import java.util.List;

/**
 * Responsible for actions on source data (Database)
 * and passing the binded depenceny with results
 */
public class EmployeeRepository {

    private final EmployeeDao employeeDao;
    private LiveData<List<Employee>> allEmployees;

    public EmployeeRepository(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
        this.allEmployees = employeeDao.getAllEmployees();
    }

    @MainThread
    public LiveData<List<Employee>> getAllEmployeesInsideThisDepartment(int departmentId) {
        return employeeDao.getEmployeeByDepartmentID(departmentId);
    }

    public void updateEmployee(Employee employee) {
        new UpdateEmployee().execute(employee);
    }


    public void insertEmployee(Employee employee) {
        new InsertNewEmployee().execute(employee);
    }


    @MainThread
    public LiveData<List<Employee>> getAllEmployees() {
        return employeeDao.getAllEmployees();
    }

    @MainThread
    public long deleteEmployeeById(Employee employee) {
        return employeeDao.deleteEmployee(employee);
    }

    private class InsertNewEmployee extends AsyncTask<Employee, Void, Long> {
        @Override
        protected Long doInBackground(Employee... employees) {
            return employeeDao.saveData(employees[0]);
        }
    }

    private class UpdateEmployee extends AsyncTask<Employee, Void, Integer> {
        @Override
        protected Integer doInBackground(Employee... employees) {
            return employeeDao.updateData(employees[0]);
        }
    }

}
