package com.example.employeetask.ui.employee;

import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;

import com.example.employeetask.R;
import com.example.employeetask.base.BaseFragment;
import com.example.employeetask.data.entity.Employee;
import com.example.employeetask.databinding.LayoutFragmentShowEmployeeBinding;
import com.example.employeetask.model.viewmodel.EmployeeViewModel;
import com.example.employeetask.ui.adapter.EmployeeListAdapter;

import java.util.List;

public class ShowEmployeeFragment extends BaseFragment<LayoutFragmentShowEmployeeBinding, EmployeeViewModel> {

    private EmployeeViewModel employeeViewModel;
    private EmployeeListAdapter employeeListAdapter;

    @Override
    public void initForStart() {
        employeeListAdapter = new EmployeeListAdapter(mViewDataBinding.recyclerView);
        mViewDataBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        mViewDataBinding.recyclerView.setAdapter(employeeListAdapter);

        employeeViewModel.getAllEmployeeList().observe(this, new Observer<List<Employee>>() {
            @Override
            public void onChanged(List<Employee> employees) {
                employeeListAdapter.setData(employees);
                employeeListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public EmployeeViewModel getViewModel() {
        employeeViewModel = new ViewModelProvider(this, new EmployeeViewModel.Factory()).
                get(EmployeeViewModel.class);
        return employeeViewModel;
    }

    @Override
    public int getLayout() {
        return R.layout.layout_fragment_show_employee;
    }

    @Override
    public void clearVariables() {

    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public Fragment getLifeCycleOwner() {
        return this;
    }
}
