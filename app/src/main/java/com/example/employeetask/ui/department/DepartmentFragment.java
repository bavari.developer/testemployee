package com.example.employeetask.ui.department;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.employeetask.BR;
import com.example.employeetask.R;
import com.example.employeetask.base.BaseFragment;
import com.example.employeetask.base.EmployeeApplication;
import com.example.employeetask.data.faker.DatabaseFaker;
import com.example.employeetask.databinding.LayoutFragmentDepartmentShowBinding;
import com.example.employeetask.model.viewmodel.DepartmentViewModel;
import com.example.employeetask.ui.adapter.DepartmentAdapter;

public class DepartmentFragment extends BaseFragment<LayoutFragmentDepartmentShowBinding, DepartmentViewModel> {

    DepartmentAdapter departmentAdapter;
    LayoutFragmentDepartmentShowBinding fragmentDepartmentShowBinding;

    @Override
    public void initForStart() {

        departmentAdapter = new DepartmentAdapter();
        mViewDataBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        mViewDataBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mViewDataBinding.recyclerView.setAdapter(departmentAdapter);


        new DatabaseFaker();
        getViewModel().getAllDepartment().observe(this,
                departments -> {
                    departmentAdapter.setData(departments);
                    departmentAdapter.notifyDataSetChanged();
                });
    }

    @Override
    public DepartmentViewModel getViewModel() {
        DepartmentViewModel.Factory factory = new DepartmentViewModel.Factory(EmployeeApplication.getInstance());
        return new ViewModelProvider(getLifeCycleOwner(), factory).get(DepartmentViewModel.class);
    }

    @Override
    public int getLayout() {
        return R.layout.layout_fragment_department_show;
    }

    @Override
    public void clearVariables() {
        mViewDataBinding.recyclerView.setAdapter(null);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public Fragment getLifeCycleOwner() {
        return this;
    }
}
