package com.example.employeetask.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeetask.base.BaseViewHolder;
import com.example.employeetask.data.entity.Employee;
import com.example.employeetask.databinding.CellEmployeeShowBinding;

import java.util.List;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.EmployeeViewHolder> {

    List<Employee> employeeList;
    private RecyclerView recyclerView;


    public EmployeeListAdapter(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }


    public void setData(List<Employee> employeeList) {
        this.employeeList = employeeList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EmployeeViewHolder(CellEmployeeShowBinding.inflate(LayoutInflater.from(parent.getContext())
                , parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        holder.onBind(employeeList.get(position), position);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull EmployeeViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public static class EmployeeViewHolder extends BaseViewHolder<Employee> {

        CellEmployeeShowBinding cellEmployeeShowBinding;

        public EmployeeViewHolder(CellEmployeeShowBinding cellEmployeeShowBinding) {
            super(cellEmployeeShowBinding.getRoot());
            this.cellEmployeeShowBinding = cellEmployeeShowBinding;
        }

        @Override
        public void onBind(Employee data, int position) {
            cellEmployeeShowBinding.tvAddress.setText(data.getAddress());
            cellEmployeeShowBinding.tvEmail.setText(data.getEmail());
        }
    }


}
