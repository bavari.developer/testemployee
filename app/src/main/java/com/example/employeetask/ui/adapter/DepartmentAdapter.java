package com.example.employeetask.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.employeetask.base.BaseViewHolder;
import com.example.employeetask.data.entity.Department;
import com.example.employeetask.databinding.DepartmentCellBinding;
import com.example.employeetask.model.viewmodel.DepartmentViewModel;

import java.util.ArrayList;
import java.util.List;

public class DepartmentAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    BaseViewHolder.RecyclerClick<Department> recyclerClick;
    public DepartmentViewModel departmentViewModel;
    List<Department> localDepartmentList = new ArrayList<>();

    public DepartmentAdapter() {
        this.departmentViewModel = new DepartmentViewModel();
    }

    public void setData(List<Department> departmentList) {
        if (this.localDepartmentList.size() > 0)
            this.localDepartmentList.clear();
        localDepartmentList.addAll(departmentList);
    }

    @NonNull
    @Override
    public DepartmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DepartmentViewHolder(DepartmentCellBinding.inflate(LayoutInflater.
                from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(localDepartmentList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return this.localDepartmentList.size();
    }

    public static class DepartmentViewHolder extends BaseViewHolder<Department> {

        DepartmentCellBinding bindingVariable;

        public DepartmentViewHolder(DepartmentCellBinding itemView) {
            super(itemView.getRoot());
            this.bindingVariable = itemView;
        }

        @Override
        public void onBind(Department department, int position) {
            bindingVariable.setViewModel(department);
//            bindingVariable.tvId.setText(department.getIdAsString());
//            bindingVariable.setViewModel(departmentViewModel);
        }
    }
}
