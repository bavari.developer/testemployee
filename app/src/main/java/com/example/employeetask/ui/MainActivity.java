package com.example.employeetask.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.employeetask.BR;
import com.example.employeetask.R;
import com.example.employeetask.base.EmployeeApplication;
import com.example.employeetask.databinding.ActivityMainBinding;
import com.example.employeetask.model.viewmodel.DepartmentViewModel;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private DepartmentViewModel departmentViewModel;
    public NavController navController;
    ActivityMainBinding mViewDataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewDataBinding = initViewBindingAndModel();
        setSupportActionBar(mViewDataBinding.toolbar);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.mobile_navigation)
                .setDrawerLayout(mViewDataBinding.drawerLayout)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.setGraph(R.navigation.mobile_navigation);
        NavigationUI.setupWithNavController(mViewDataBinding.toolbar, navController, mAppBarConfiguration);
    }

    private ActivityMainBinding initViewBindingAndModel() {
        // establish custom facotry for the purpose of creating new modelView
        DepartmentViewModel.Factory factory = new DepartmentViewModel.Factory(EmployeeApplication.getInstance());
        departmentViewModel = new ViewModelProvider(this, factory).get(DepartmentViewModel.class);
        ActivityMainBinding mViewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewDataBinding.setVariable(BR.activityModel, departmentViewModel);
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.executePendingBindings();
        return mViewDataBinding;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}