package com.example.employeetask.model.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.employeetask.base.EmployeeApplication;
import com.example.employeetask.data.entity.Department;
import com.example.employeetask.data.repository.DepartmentRepository;

import java.util.List;

/**
 * The basic operations will be getting from caller Fragment / Activity
 * That's why there is no definitions here ...
 * If you need to use context inside your ViewModel you should use AndroidViewModel because
 * it contains the application context (to retrieve the context call getApplication() ),
 * otherwise, use regular ViewModel.
 */
public class DepartmentViewModel extends ViewModel {

    private DepartmentRepository employeeRepository;
    public LiveData<Department> departmentLiveData;

    public DepartmentViewModel() {
        this.employeeRepository = new DepartmentRepository(EmployeeApplication.getDataBaseInstance().getDepartment());
    }


    public LiveData<Department> getDepartmentLiveData() {
        return departmentLiveData;
    }

    public LiveData<Department> getDepartmentById(int departmentId) {
        return employeeRepository.getDepartmentById(departmentId);
    }

    public LiveData<List<Department>> getAllDepartment() {
        return employeeRepository.getAllDepartments();
    }


    public void addNewDepartment() {

    }

    public void updateDepartment(Department department) {
        employeeRepository.updateDepartment(department);
    }

    public void insertDepartment(Department department) {
        employeeRepository.insertNewDepartment(department);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new DepartmentViewModel();
        }
    }

    public interface ClickButtonNavigation {
        void onClickButton();
    }
}
