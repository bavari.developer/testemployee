package com.example.employeetask.model.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.employeetask.base.EmployeeApplication;
import com.example.employeetask.data.entity.Employee;
import com.example.employeetask.data.repository.EmployeeRepository;

import java.util.List;

public class EmployeeViewModel extends ViewModel {

    private EmployeeRepository employeeRepository;

    public EmployeeViewModel(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public LiveData<List<Employee>> getAllEmployeeList() {
        return employeeRepository.getAllEmployees();
    }

    public LiveData<List<Employee>> getEmployeesOfThisDepartment(int departmentId) {
        return employeeRepository.getAllEmployeesInsideThisDepartment(departmentId);
    }


    public void updateEmployee(Employee employee) {
        employeeRepository.updateEmployee(employee);
    }

    public void insertDepartment(Employee employee) {
        employeeRepository.insertEmployee(employee);
    }

    // Custom Factory for this ViewModel
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private EmployeeRepository employeeRepository;

        public Factory() {
            this.employeeRepository = new EmployeeRepository(EmployeeApplication.
                    getDataBaseInstance().getEmployee());
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new EmployeeViewModel(employeeRepository);
        }
    }
}
