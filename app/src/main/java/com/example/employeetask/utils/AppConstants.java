package com.example.employeetask.utils;

public class AppConstants {

    public static final class DatabaseConstants {
        /**
         * Database Version
         */
        public static final int DB_VERSION = 1;
        /**
         * Database Name
         */
        public static final String DB_NAME = "employee_DB";
    }


}
