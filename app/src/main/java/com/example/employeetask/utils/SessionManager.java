package com.example.employeetask.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public final class SessionManager {

    private static final String PREF_NAME = "com.example.employeeTask";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String KEY_OBJECT_USER = "objUser";
    private static final String KEY_LANGUAGE = "language";
    private static final String KEY_DB_INIT = "DB_init";
    private static SharedPreferences.Editor editor;
    private static SharedPreferences mSharedPref;


    private SessionManager() {
    }

//    /**
//     * Create login session
//     */
//    public static void createUserLoginSession(Customer user) {
//        editor = mSharedPref.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(user);
//        editor.putString(KEY_OBJECT_USER, json);
//        editor.apply();
//        // Storing login value as TRUE
//        editor.putBoolean(IS_LOGIN, true);
//        // commit changes
//        editor.apply();
//    }


    public static void init(Context context) {
        if (mSharedPref == null)
            mSharedPref = context.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
    }

//    /**
//     * Get stored session data
//     */
//    public static Customer getCustomerObj() {
//        Gson gson = new Gson();
//        String json = mSharedPref.getString(KEY_OBJECT_USER, "");
//        return gson.fromJson(json, Customer.class);
//    }

//    @Nullable
//    public static String getUserToken() {
//        Gson gson = new Gson();
//        String json = mSharedPref.getString(KEY_OBJECT_USER, "");
//        try {
//            return gson.fromJson(json, Customer.class).getToken();
//        } catch (NullPointerException nu) {
//            nu.printStackTrace();
//            return null;
//        }
//    }


//    /**
//     * Clear session details
//     */
//    public static void logoutUser() {
//        editor = mSharedPref.edit();
//        Customer.setObjCustomer(null);
//        editor.remove(KEY_OBJECT_USER);
//        String oldToken = getKeyFirebaseToken();
//        editor.clear();
//        editor.apply();
//        saveFireBaseToken(oldToken);
//    }


    public static boolean isLoggedIn() {
        return mSharedPref.getBoolean(IS_LOGIN, false);
    }

    public static String getLanguage() {
        return mSharedPref.getString(KEY_LANGUAGE, "en");
    }

    public static String getisSetLanguage() {
        return mSharedPref.getString(KEY_LANGUAGE, "");
    }

    public static void setLanguage(String language) {
        editor = mSharedPref.edit();
        editor.putString(KEY_LANGUAGE, language);
        editor.apply();
    }

    public static synchronized void setValue(String key, String language) {
        editor = mSharedPref.edit();
        editor.putString(key, language);
        editor.apply();
    }


    public static synchronized void setValue(String key, boolean language) {
        editor = mSharedPref.edit();
        editor.putBoolean(key, language);
        editor.apply();
    }

    public static synchronized void setValue(String key, int language) {
        editor = mSharedPref.edit();
        editor.putInt(key, language);
        editor.apply();
    }

    /**
     * Set DB init to true - this is because we need this operation only once.
     */
    public static void setDBInit() {
        setValue(KEY_DB_INIT, true);
    }

    /**
     * Special Key to retrieve the Result of Saving DB INIT inside Shared Preferences
     *
     * @return boolean result
     */
    public static boolean getDBInit() {
        return mSharedPref.getBoolean(KEY_DB_INIT, false);
    }
}